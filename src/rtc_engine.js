define([
    './model'
], function() {
    /////////////////
    function rtc_engine(){
	this.sinal = "ws";
	this.rooms = [];
	this.mcu_uri = "";
    };
    
    rtc_engine.prototype.init= function(uri,credential,options,succ,fail){
	this.uri = uri;
	this.options = options;
	var ws = new WebSocket(uri);
	ws.onopen = function(msg){
	    var req = {credential:credential};
	    ws.send(req);
	};
	ws.onfail = function(){
	    fail({code:401,msg:"connect error"});	    
	};
	ws.onmessage = function(msg){
	    if(msg.credential){
		this.mcu_uri = msg.mcu_uri;
		this.room_credetial = msg.credential;
		succ();
	    }else{
		fail({msg:"error"});
	    }
	};
    };
    
    rtc_engine.prototype.create_room = function(room_name,participater_name,succ,fail){
	var room = new rtc_room(room_name,mcu_uri);
	var my = new rtc_participater(participater_name);
	room.add_participater(my);
	room.onconnectsucc = function(){
	    succ(room);
	};
	room.onconnectfail = function(reason){
	    fail({reason:reason});
	};
	room.connect();
    };
    
    
    ////////////////
});
