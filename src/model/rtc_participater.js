define([
], function() {
    /////////////////
    function rtc_participater(name){
	this.name = name;
	this.webcams = [];
    };
    
    rtc_participater.prototype.add_webcam = function(webcam){
	this.webcams.push(webcam);
    };
    rtc_participater.prototype.sendPubMessage = function(msg){
	var req = {stream:"iq",
		   from:this.room_pid,
		   to:this.room_no,
		   message:msg};
	this.transport.send(req);
    };
    
    rtc_participater.prototype.onmessage = function(msg){
	if(msg.type=="onPubMessage"){
	    if(this.onPubMessage){
		this.onPubMessage(msg.message);
	    }
	}else if(msg.type == "onPrivateMessage"){
	    if(this.onPrivateMessage){
		this.onPubMessage(msg.from,msg.message);
	    }
	}else{
	    console.log("not support message ",msg);
	}
    };
    
    ////////////////
});
